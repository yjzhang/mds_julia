#!/usr/bin/env bash

julia main.jl -o pro_shortest_paths.txt -r 1.3 --shortest-paths  data_fish/combined_You_Pro_merged_chr4.csv

julia main.jl -f data_fish/chr4_qui_probes.txt --auto-scale -o qui.txt data_fish/combined_Young_Qui_merged_chr4.csv

julia main.jl -f data_fish/chr4_sen_probes.txt --auto-scale -o sen.txt data_fish/combined_Sen_merged_chr4.csv

julia main.jl -f data_fish/chr4_sen_probes.txt -o sen_no_scale.txt data_fish/combined_Sen_merged_chr4.csv

julia main.jl -o pro_rad_scale.txt -r 1.3 data_fish/combined_You_Pro_merged_chr4.csv
