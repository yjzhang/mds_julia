import numpy as np
import os
from mayavi.mlab import *

#readPDB(fname, resolution, plusX): returns [[xcoordinates],[ycoordinates],[zcoordinates]]
# for a pdb file specified in fname.
# resolution must be one of string {1m, 200k, 40k} to read propper file
# if plusX!=none, adds the value to the x coordinates to separate things out some. 
def readPDB(fname, resolution, plusX=None):
    x=[]
    y=[]
    z=[]
    with open(fname,'r') as pdb:
        line = pdb.readline()
        # check for empty line
        if line=='':
            return [x,y,z]
        elif resolution=='40k':
            while line != '':
                x.append(float(line[31:38]))
                y.append(float(line[39:46]))
                z.append(float(line[47:54]))
                line=pdb.readline()
        else:
            while line != '':
                x.append(float(line[30:37]))
                y.append(float(line[38:45]))
                z.append(float(line[46:53]))
                line=pdb.readline()
    if plusX !=None:
        x = [a+plusX for a in x]

    x=np.array(x)
    y=np.array(y)
    z=np.array(z)
    return [x,y,z]




#readEigen(fname): returns a list of eigenvector values, rescaled to match length, from a AB Compartment bedfile produced
# by my hic pipeline. Only reads for a given chromosome (1 indexed). If arm!=None, only reads for a given arm (1 or 2)
# does not read lines that have an eigenvector of 0 (removed rows)
def readEigen(fname, length, chrom, arm=None):
    # check for empty arm
    if length==0:
        return []

    with open(fname, 'r') as bf:
        #skip first line
        discard=bf.readline()
        lines=[b.strip().split() for b in bf.readlines()]
    if arm!=None:
        eigen = [float(b[5]) for b in lines if b[0]=='chr' + str(chrom) and b[4]==str(arm) and b[5]!='0']
    else:
        eigen = [float(b[3]) for b in lines if b[0]=='chr' + str(chrom) and b[3]!='0']

    #recast to right shape
    #ratio between lengths
    ratio=length/float(len(eigen))
    eigen_reshape=[]
    for i in range(length):
        eigen_reshape.append(eigen[int(i/ratio)])    

    return eigen_reshape

def readColors(filename, length, chrom):
    with open(filename) as f:
        pass

# code for animation and save to png sequence 
@animate(delay=10)
def anim():
    f = gcf()
    i=0
    while i<500:
        f.scene.camera.azimuth(0.75)
        f.scene.render()
        f.scene.save('movies/senV_chr1_'+"%05d" % i+'.png')
        i+=1
        print i
        yield

#a = anim() # Starts the animation.


@animate(delay=10)
def anim2():
    f1 = engine.scenes[21]
    f2 = engine.scenes[20]
    i=0
    while i<500:
        f1.scene.camera.azimuth(0.75)
        f1.scene.render()
        f1.scene.save('pastis/movies/SEN_chr1_'+"%05d" % i+'.png')
        f2.scene.camera.azimuth(0.75)
        f2.scene.render()
        f2.scene.save('pastis/movies/QUI_chr1_'+"%05d" % i+'.png')
        i+=1
        print i
        yield

def simple_mayavi_vis(filename, res, ev = None):
    data = readPDB(filename, res)
    if ev is None:
        plot3d(data[0], data[1], data[2], tube_radius=1, colormap='RdBu')
    else:
        eig = readEigen(ev, len(data[0]), 18)
        print len(data)
        print len(eig)
        plot3d(data[0], data[1], data[2], eig,tube_radius=1, colormap='RdBu')
    show()
    #anim()
    #savefig('plot.png')

if __name__ == '__main__':
    data = readPDB('chr4_sen_arm1_with_constraint.txt', '200k')
    plot3d(data[0], data[1], data[2], colormap='RdBu')
    #simple_mayavi_vis('test.pdb', '200k')
