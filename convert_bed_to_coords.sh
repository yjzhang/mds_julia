#!/bin/sh

bedtools intersect -wa -f 1 -a bins_200k.bed -b LADs_A.bed > intersected_200k_A.bed

grep chr18 bins_200k.bed | head -1

grep chr18 intersected_200k_A.bed > intersected_200k_char18.bed

python convert_to_npy.py intersected_200k_char18.bed 12914 lads_chr18.txt
