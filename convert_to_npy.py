#!/usr/bin/env python

import numpy as np
import sys

def convert_csv_to_npy(in_file, outfile):
    data = np.loadtxt(in_file, delimiter=',')
    np.save(outfile, data)

def convert_bed_to_list(bedfile, start_index=0, outfile='lads_out.txt'):
    """
    bedfile - a bed file containing only the lads of a given chromosome.

    start_index - the start bin number
    """
    with open(bedfile) as b:
        out = open(outfile, 'w')
        out_nums = []
        for line in b.readlines():
            data = line.split()
            num = int(data[3]) - start_index
            out_nums.append(str(num))
        out.write(str(len(out_nums)) + '\n')
        out.write('\n'.join(out_nums))
        out.close()

if __name__ == '__main__':
    #infile = sys.argv[0]
    #out = sys.argv[1]
    #convert_csv_to_npy(infile, out)
    convert_bed_to_list(sys.argv[1], int(sys.argv[2]), sys.argv[3])
