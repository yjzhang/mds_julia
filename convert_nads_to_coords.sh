#!/bin/sh

bedtools intersect -wa -f 1 -a bins_200k.bed -b nucleolus_domains_hg18.txt > intersected_200k_nad_hg18.bed

grep chr18 intersected_200k_nad_hg18.bed | head -1

grep chr18 intersected_200k_nad_hg18.bed > intersected_200k_nads_char18.bed

python convert_to_npy.py intersected_200k_nads_char18.bed 12914 nads_chr18.txt
